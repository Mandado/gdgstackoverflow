<?php
require 'vendor/autoload.php';
ini_set('display_errors',1);
error_reporting(E_ALL);
session_start();

$client = new GuzzleHttp\Client(['base_uri' => 'https://stackexchange.com/']);
$StackOverflowApi = new Api\StackOverflow($client);
// idsStackOverflow
$StackOverflowApi->run([3612965, 448292]);
echo json_encode($StackOverflowApi->getUsersInfo());
