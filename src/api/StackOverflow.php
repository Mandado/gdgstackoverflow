<?php
namespace Api;
use GuzzleHttp\Client as Client;

class StackOverflow{

  private $client;
  private $defaultOptions = [];
  private $urlApi = 'https://api.stackexchange.com/2.2';
  private $idsStackExchange = [];
  private $userData = [];

  public function __construct(Client $client){
    $this->client = $client;
  }

  public function authenticate(){
    $response = $this->client->request('GET','oauth',[
      'query' => [
        'client_id' => '5907',
        'scope' => 'no_expiry',
        'redirect_uri' => 'http://localhost:8000'
      ]
    ]);

    echo $response->getBody();
  }

  public function getAccessToken(){
    $response = $this->client->request('POST','oauth/access_token',[
      'form_params' => [
        'client_id' => '5907',
        'client_secret' => 'Eq5xdqvgPB3XefGxsbetaw((',
        'code' => $_GET['code'],
        'redirect_uri' => 'http://localhost:8000'
      ]
    ]);

    return $response->getBody();
  }

  public function getIdsStackExchange(array $idsStackOverflow = []){
    $response = $this->request('/?order=desc&sort=reputation&site=stackoverflow', $idsStackOverflow);
    $json  = json_decode($response->getBody());

    // pega id stackexchange
    $this->idsStackExchange = array_map(function($item){
      return $item->account_id;
    }, $json->items);

  }

  public function requestTags(){
    $responsesTag = $this->request('/tags?order=desc&sort=popular&site=stackoverflow');
    $json  = json_decode($responsesTag->getBody());

    foreach($json->items as $item){
      if(in_array($item->user_id, $this->idsStackExchange)){
         $this->userData[$item->user_id]['tags'][] = $item->name;
      }
    };
  }

  public function requestInfoUsers(){
    $response = $this->request('/associated?filter=!9YdnSG37z');
    $json  = json_decode($response->getBody());

    //account_id é id do exchange
    foreach($json->items as $item){
      if(in_array($item->account_id, $this->idsStackExchange)){
         $this->userData[$item->account_id]['networks'][] = [
           'question_count'=>  $item->question_count,
           'answer_count'=>  $item->answer_count,
           'account_id' =>  $item->account_id,
           'reputation' =>  $item->reputation,
           'user_id' =>  $item->user_id,
           'site_name' => $item->site_name,
         ];
      }
    };
  }

  public function getUsersInfo(){
    return $this->userData;
  }


  public function run($idsStackOverflow = []){
    if(!isset($_GET['code'])){
      return $this->authenticate();
    }

    parse_str($this->getAccessToken());

    $this->defaultOptions = [
      'form_params' => [
        'access_token' => $access_token,
        'key' => 'h8fKAQ)eOOhyegaHfHFlIg(('
      ]
    ];

    $this->getIdsStackExchange($idsStackOverflow);
    $this->requestTags();
    $this->requestInfoUsers();
  }

  private function getUrl($url, $idsStackOverflow = []){
    $ids = join(';', (empty($idsStackOverflow)) ? $this->idsStackExchange : $idsStackOverflow);
    return sprintf("{$this->urlApi}/users/%s%s", $ids, $url);
  }

  private function request($url, $idsStackOverflow = []){
    return $this->client->request('GET',$this->getUrl($url, $idsStackOverflow), $this->defaultOptions);
  }

}
